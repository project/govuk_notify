<?php

namespace Drupal\govuk_notify\NotifyService;

use Alphagov\Notifications\Client as AlphagovClient;
use Alphagov\Notifications\Exception\ApiException;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Http\Adapter\Guzzle7\Client;

/**
 * Service class for GovUK Notify.
 */
class GovUKNotifyService implements NotifyServiceInterface {

  /**
   * Notify client.
   *
   * @var \Alphagov\Notifications\Client|null
   */
  protected $notifyClient = NULL;

  /**
   * Create the GovUK notify API client.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected LoggerChannelInterface $loggerChannel,
    protected CacheBackendInterface $cacheBackend,
  ) {
    $config = $this->configFactory->get('govuk_notify.settings');

    if (!$config->get('notification_service')) {
      $notification_service = 'uk';
    }
    else {
      $notification_service = $config->get('notification_service');
    }

    switch ($notification_service) {
      case 'au':
        $url = "https://rest-api.notify.gov.au";
        break;

      case 'ca':
        $url = "https://api.notification.alpha.canada.ca";
        break;

      default:
        $url = "https://api.notifications.service.gov.uk";
        break;
    }

    try {
      $this->notifyClient = new AlphagovClient([
        'baseUrl' => $url,
        'apiKey' => $config->get('api_key'),
        'httpClient' => new Client(),
      ]);
    }
    catch (ApiException $e) {
      $this->loggerChannel->warning("Failed to create Gov Notify Client using API: @message",
        ['@message' => $e->getMessage()]);
    }
    catch (\Exception $e) {
      $this->loggerChannel->warning("Failed to create Gov Notify Client using API: @message",
        ['@message' => $e->getMessage()]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendEmail($to, $template_id, $params) {

    try {
      if (!$this->notifyClient) {
        throw new Exception("Gov Notify notifyClient not set");
      }
      return $this->notifyClient->sendEmail($to, $template_id, $params);
    }
    catch (ApiException $e) {
      $this->loggerChannel->warning("Failed to send email using API: @message",
        ['@message' => $e->getMessage()]);
    }
    catch (\Exception $e) {
      $this->loggerChannel->warning("Failed to send email using API: @message",
        ['@message' => $e->getMessage()]);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function sendSms($to, $template_id, $params) {

    try {
      if (!$this->notifyClient) {
        throw new Exception("Gov Notify notifyClient not set");
      }
      return $this->notifyClient->sendSms($to, $template_id, $params);
    }
    catch (ApiException $e) {
      $this->loggerChannel->warning("Failed to send text message using API: @message",
        ['@message' => $e->getMessage()]);
    }
    catch (\Exception $e) {
      $this->loggerChannel->warning("Failed to send text message using API: @message",
        ['@message' => $e->getMessage()]);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate($template_id) {
    if (!$this->notifyClient) {
      $this->loggerChannel->warning("Can't get template - no notifyClient");
      return NULL;
    }
    $template = &drupal_static(__FUNCTION__);
    if (is_null($template)) {

      $cache = $this->cacheBackend->get("govuk_notify_template:{$template_id}");
      if ($cache) {
        $template = $cache->data;
      }
      else {
        try {
          $template = $this->notifyClient->getTemplate($template_id);
          $this->cacheBackend->set("govuk_notify_template:{$template_id}", $template, Cache::PERMANENT, ['govuk_notify_template:' . $template_id]);
        }
        catch (ApiException $e) {
          $this->loggerChannel->warning("Failed to get a template using API: @message",
            ['@message' => $e->getMessage()]);
        }
        catch (\Exception $e) {
          $this->loggerChannel->warning("Failed to get a template using API: @message",
            ['@message' => $e->getMessage()]);
        }

        return $template ?? [];
      }

    }

    return $template;
  }

  /**
   * {@inheritdoc}
   */
  public function checkReplacement($value, $replacement) {
    return strpos($value, "(($replacement))") !== FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function listNotifications($filter = []) {
    return $this->notifyClient->listNotifications($filter);
  }

}
