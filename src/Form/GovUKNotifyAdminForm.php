<?php

namespace Drupal\govuk_notify\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Admin form for GovUK Notify settings.
 */
class GovUKNotifyAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    protected MailManagerInterface $mailManager,
    protected AccountProxyInterface $currentUser,
  ) {
    parent::__construct($configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.mail'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'govuk_notify.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'govuk_notify_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormTitle() {
    return 'Gov Notify Configuration Settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('govuk_notify.settings');

    $form['notification_service'] = [
      '#type' => 'select',
      '#default_value' => $config->get('notification_service'),
      '#title' => $this->t('Select the notification service to use'),
      '#options' => [
        'uk' => $this->t('Gov.UK Notify Service'),
        'ca' => $this->t('Government of Canada Notification Service'),
        'au' => $this->t('Australian Government Notify Service'),
      ],
    ];

    $form['introduction'] = [
      '#markup' => '<div>For detailed installation and setup instructions please see <a href="https://www.drupal.org/docs/8/modules/govuk-notify/installation">https://www.drupal.org/docs/8/modules/govuk-notify/installation</a></div>',
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('The API key to use. You can generate an API key by logging in to your Gov Notify Service and going to the API integration page.'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['default_template_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default email template ID'),
      '#description' => $this->t('The template ID to use for emails if one is not specified. This template should have one single placeholder for the subject, ((subject)), and one in the message body ((message))'),
      '#default_value' => $config->get('default_template_id'),
    ];

    $form['default_sms_template_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default SMS Text template ID'),
      '#description' => $this->t('The template ID to use for SMS Text messages if one is not specified. This template should have one single placeholder for the message body ((message))'),
      '#default_value' => $config->get('default_sms_template_id'),
    ];

    $mail_interfaces = $this->config('system.mail')->get('interface');
    $form['send_system_emails'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Gov Notify to send system emails'),
      '#description' => $this->t("Ensures that all system emails are sent using Gov Notify"),
      '#default_value' => ($mail_interfaces['default'] == 'govuk_notify_mail'),
    ];

    $form['force_temporary_failure'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always force a temporary failure'),
      '#description' => $this->t("Ensures that all emails return a temporary failure. Requires that the API key is a test key (see https://www.notifications.service.gov.uk/integration_testing)."),
      '#default_value' => $config->get('force_temporary_failure'),
    ];

    $form['force_permanent_failure'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always force a permanent failure'),
      '#description' => $this->t("Ensures that all emails return a permanent failure. Requires that the API key is a test key (see https://www.notifications.service.gov.uk/integration_testing)."),
      '#default_value' => $config->get('force_permanent_failure'),
    ];

    $form['govuk_notify_email_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test email address'),
      '#description' => $this->t('If you enter an email address into this field, the system will attempt to send an email to that address using the gov notify service. You can use this to test that you have entered the correct API key.'),
    ];

    $form['govuk_notify_sms_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test phone number for SMS'),
      '#description' => $this->t('If you enter a phone number into this field, the system will attempt to send an SMS to that phone the gov notify service. You can use this to test that you have entered the correct API key.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * If the gov_notify_email_test field has been completed we try to send an
   * email to the default template.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('govuk_notify.settings')
      ->set('notification_service', $form_state->getValue('notification_service'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('default_template_id', $form_state->getValue('default_template_id'))
      ->set('default_sms_template_id', $form_state->getValue('default_sms_template_id'))
      ->set('force_temporary_failure', $form_state->getValue('force_temporary_failure'))
      ->set('force_permanent_failure', $form_state->getValue('force_permanent_failure'))
      ->save();
    Cache::invalidateTags(["govuk_notify_template:{$form_state->getValue('default_template_id')}"]);

    // Set the default mail interface to govuk_notify, if that's what we want.
    // Or if it was previously set to govuk_notify and we no longer want to use
    // that for default messages then set it back to php_mail.
    $mail_interfaces = $this->config('system.mail')->get('interface');
    if ($form_state->getValue('send_system_emails')) {
      $mail_interfaces['default'] = 'govuk_notify_mail';
      $this->configFactory->getEditable('system.mail')->set('interface', $mail_interfaces)->save();
    }
    else {
      if (isset($mail_interfaces['default']) && $mail_interfaces['default'] == 'govuk_notify_mail') {
        $mail_interfaces['default'] = 'php_mail';
        $this->configFactory->getEditable('system.mail')->set('interface', $mail_interfaces)->save();
      }
    }

    if (!empty($form_state->getValue('govuk_notify_email_test'))) {
      $to = $form_state->getValue('govuk_notify_email_test');
      $langcode = $this->currentUser()->getPreferredLangcode();
      $params['subject'] = $this->t("This is a test message from the Gov Notify Drupal module.");
      $params['message'] = $this->t("This is a test message. If you have received this message the Gov Notify Drupal module is working successfully");
      $send = TRUE;
      $result = $this->mailManager->mail('govuk_notify', NULL, $to, $langcode, $params, NULL, $send);
      if ($result['result'] === FALSE) {
        $this->messenger()->addMessage($this->t('There was a problem sending your test email to %email and it was not sent.', ['%email' => $to]), 'error');
      }
      else {
        $this->messenger()->addMessage($this->t('Test email has been sent.'));
      }
    }

    if (!empty($form_state->getValue('govuk_notify_sms_test'))) {
      $to = $form_state->getValue('govuk_notify_sms_test');
      $langcode = $this->currentUser()->getPreferredLangcode();
      $params['message'] = $this->t("This is a test SMS message. If you have received this message the Gov Notify Drupal module is working successfully");
      $send = TRUE;
      $result = $this->mailManager->mail('govuk_notify', NULL, $to, $langcode, $params, NULL, $send);
      if ($result['result'] === FALSE) {
        $this->messenger()->addMessage($this->t('There was a problem sending your test SMS to %number and it was not sent.', ['%number' => $to]), 'error');
      }
      else {
        $this->messenger()->addMessage($this->t('Test SMS has been sent.'));
      }
    }
  }

}
