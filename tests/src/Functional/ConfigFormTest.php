<?php

declare(strict_types=1);

namespace Drupal\Tests\govuk_notify\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the module's configuration form.
 */
class ConfigFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['govuk_notify'];

  /**
   * Test configuration form protected by permissions.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testConfigFormProtected() {
    // Test anonymous users can't access the configuration screen.
    $this->drupalGet('admin/config/system/govuk_notify');
    $this->assertSession()->statusCodeEquals(403);

    // Test a logged in user with no specific permissions can't access the
    // configuration screen.
    $person_of_no_significance = $this->createUser();
    $this->drupalLogin($person_of_no_significance);
    $this->drupalGet('admin/config/system/govuk_notify');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();

    // Test a logged in user with the appropriate permission can access the
    // configuration screen.
    $person_of_appropriate_significance = $this->createUser(['administrator gov uk notify']);
    $this->drupalLogin($person_of_appropriate_significance);
    $this->drupalGet('admin/config/system/govuk_notify');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalLogout();
  }

}
